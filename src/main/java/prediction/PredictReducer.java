package prediction;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

import static utils.ComUtils.split;

public class PredictReducer extends Reducer<Text, Text, Text, Text> {

    Text v = new Text();

    @Override
    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        String max_class = "";
        double max_prob = Double.NEGATIVE_INFINITY;  // -1.0/0.0  负无穷大
        for (Text value : values) {
            String[] arr = split(value.toString(),"\t");
            if (Double.parseDouble(arr[1]) > max_prob){
                max_class = arr[0];
                max_prob = Double.parseDouble(arr[1]);
            }
        }

        v.set(max_class);
        context.write(key,v);
    }
}
