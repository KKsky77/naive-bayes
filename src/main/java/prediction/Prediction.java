package prediction;

import java.io.*;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class Prediction {

    private static Hashtable<String,Double> class_prob = new Hashtable<>();
    private static Hashtable<Map<String,String>,Double> class_term_prob = new Hashtable<>();
    private static Hashtable<String,Double> class_term_total = new Hashtable<>();
    private static Hashtable<String,Double> class_term_num = new Hashtable<>();

    public Prediction(String path1, String path2) throws IOException {
        init(path1, path2);
    }

    private void init(String path1,String path2) throws IOException{
        // 统计文档总数
        BufferedReader reader = new BufferedReader(new FileReader(path1 + "/" + "part-r-00000"));
        double file_total = 0;
        while (reader.ready()) {
            String line = reader.readLine();
            String[] args = line.split("\t");
            file_total += Double.parseDouble(args[1]);
        }
        System.out.println(file_total);
        // 计算先验概率class_prob
        reader = new BufferedReader(new FileReader(path1 + "/" + "part-r-00000"));
        while (reader.ready()) {
            String line = reader.readLine();
            String[] args = line.split("\t");
            class_prob.put(args[0], Double.parseDouble(args[1]) / file_total);
            System.out.printf(("%s:%f%n"), args[0], Double.parseDouble(args[1]) / file_total);
        }
        //计算单词总数
        reader = new BufferedReader(new FileReader(path2 + "/" + "part-r-00000"));
        while (reader.ready()) {
            String line = reader.readLine();
            String[] args = line.split("\t");// 0：类，1：词条，2：词频
            double count = Double.parseDouble(args[2]);
            String classname = args[0];
            class_term_total.put(classname, class_term_total.getOrDefault(classname, 0.0) + count);
        }
        System.out.println(class_term_total);
        //计算单词集合大小
        reader = new BufferedReader(new FileReader(path2 + "/" + "part-r-00000"));
        while (reader.ready()) {
            String line = reader.readLine();
            String[] args = line.split("\t");// 0：类，1：词条，2：词频
            String classname = args[0];
            class_term_num.put(classname, class_term_num.getOrDefault(classname, 0.0) + 1.0);
        }
        System.out.println(class_term_num);
        //计算每个类别里面出现的词条概率class-term prob
        reader = new BufferedReader(new FileReader(path2 + "/" + "part-r-00000"));
        while (reader.ready()) {
            String line = reader.readLine();
            String[] args = line.split("\t");// 0：类，1：词条，2：词频
            double count = Double.parseDouble(args[2]);
            String classname = args[0];
            String term = args[1];
            Map<String, String> map = new HashMap<>();
            map.put(classname, term);
            class_term_prob.put(map, (count + 1) / (class_term_total.get(classname) + class_term_num.get(classname)));
        }
    }

    public static double calculate(String content,String classname){
        // 计算一个文档属于某类的条件概率
        double result = 0;
        String[] words = content.split("\n");
        for(String word:words){
            Map<String,String> map = new HashMap<>();
            map.put(classname, word);
            result += Math.log(class_term_prob.getOrDefault(map,1.0/(class_term_total.get(classname)+class_term_num.get(classname))));
        }
        result += Math.abs(Math.log(class_prob.get(classname)));
        return result;
    }


    public static void main(String[] args) throws IOException {
        new Prediction("data/output","data/output1");
    }
}
