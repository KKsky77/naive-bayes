package prediction;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;

import static utils.ComUtils.split;

public class PredictMapper extends Mapper<Text, Text, Text, Text> {

    Text k = new Text();
    Text v = new Text();
    String dir;
    String filename;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Path path = ((FileSplit) context.getInputSplit()).getPath();
        dir = path.getParent().getName();
        filename = path.getName();
    }

    @Override
    protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {
        String[] CLASS_NAMES = {"CHINA","CANA"};
        for (String class_name : CLASS_NAMES) {
            k.set(dir + "@" + filename);
            double prob = Prediction.calculate(value.toString(),class_name);
            v.set(class_name + "\t" + prob);
            context.write(k,v);
        }
    }
}

