package doc;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import utils.WholeFileInputFormat;

import java.util.List;

public class DocDriver {
    public static void run(List<Path> paths, String out) throws Exception {

        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf);
        job.setJarByClass(DocDriver.class);

        job.setMapperClass(DocMapper.class);
        job.setReducerClass(DocReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setCombinerClass(DocReducer.class);
        job.setInputFormatClass(WholeFileInputFormat.class);


        Path outputPath = new Path(out);

        for (Path path : paths) {
            FileInputFormat.addInputPath(job,path);
        }
        FileOutputFormat.setOutputPath(job, outputPath);

        FileSystem fs = FileSystem.get(conf);

        if (fs.exists(outputPath)){
            fs.delete(outputPath,true);
        }

        job.waitForCompletion(true);

    }

}
