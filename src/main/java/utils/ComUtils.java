package utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.util.*;

public class ComUtils {

    public static List<Path> train = new ArrayList<>();
    public static List<Path> test = new ArrayList<>();

    public static String[] split(String str, String split) {
        return StringUtils.splitByWholeSeparatorPreserveAllTokens(str, split);
    }

    public static  void train_test(String path) {

        Map<String, File[]> map = new HashMap<>();

        File file_path = new File(path);
        File[] file_paths = file_path.listFiles();
        assert file_paths != null;

        String[] _class = new String[file_paths.length];
        int i = 0;
        for (File files : file_paths) {
            File[] all_file = files.listFiles();
            map.put(files.getName(),all_file);
            _class[i] = files.getName();
            i++;
        }


        for (String name : _class) {
            String _names = Arrays.toString(map.get(name)).replace("[","").replace("]","");
            List<String> arr = Arrays.asList(split(_names,","));
            Collections.shuffle(arr);

            for (int j = 0; j < arr.size(); j++) {
                if (j < arr.size() * 0.7){
                    train.add(new Path(arr.get(j).trim()));
                }else {
                    test.add(new Path(arr.get(j).trim()));
                }
            }
        }

    }
}
