
import count.CountDriver;
import doc.DocDriver;
import org.apache.hadoop.fs.Path;
import prediction.Evaluation;
import prediction.PredictDriver;
import prediction.Prediction;
import utils.ComUtils;

import java.util.List;

import static utils.ComUtils.*;

public class Main {
    public static void main(String[] args) throws Exception {
        String input_path = "data/input";

        // 拆分训练数据和测试数据
        train_test(input_path);

        List<Path> train = ComUtils.train;
        List<Path> test = ComUtils.test;

        DocDriver.run(train,"data/tmp1");
        CountDriver.run(train,"data/tmp2");

        new Prediction("data/tmp1", "data/tmp2");

        PredictDriver.run(test,"data/tmp3");


        Evaluation.main(new String[]{"data/tmp3/part-r-00000"});

    }
}
