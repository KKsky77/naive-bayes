package count;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.IOException;

import static utils.ComUtils.split;

public class CountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

    Text k = new Text();
    String dir;
    IntWritable v = new IntWritable(1);

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Path path = ((FileSplit) context.getInputSplit()).getPath();
        dir = path.getParent().getName();
    }
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] words = split(value.toString(),"\n");
        for (String word : words) {

            k.set(dir + "\t" + word);
            context.write(k,v);
        }

    }
}

